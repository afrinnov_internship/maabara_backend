package com.afrinnov.maabara.patient;

import com.afrinnov.maabara.commons.AbstractMapper;
import static com.afrinnov.maabara.commons.PrefixCode.PATIENT;
import com.afrinnov.maabara.commons.Util;
import org.springframework.stereotype.Component;

@Component
public class PatientMapper extends AbstractMapper<Patient, PatientDto, PatientForm> {
    @Override
    public Patient fromDtoToEntity(PatientDto patientDto) {
        Patient patient = new Patient();
        patient.setSex(patientDto.getSex());
        patient.setPhone(patientDto.getPhone());
        patient.setDateBirth(patientDto.getDateBirth());
        patient.setSurname(patientDto.getSurname());
        patient.setName(patientDto.getName());
        patient.setCode(patientDto.getPatientCode());
        patient.setWeight(patientDto.getWeight());
        patient.setTension(patientDto.getTension());
        patient.setTemperature(patientDto.getTemperature());
        patient.setDeleted(patientDto.isDeleted());
        return patient;
    }

    @Override
    public Patient CopyDtoToEntity(Patient patient, PatientDto patientDto) {
        patient.setSex(patientDto.getSex());
        patient.setPhone(patientDto.getPhone());
        patient.setDateBirth(patientDto.getDateBirth());
        patient.setSurname(patientDto.getSurname());
        patient.setName(patientDto.getName());
        patient.setCode(patientDto.getPatientCode());
        patient.setWeight(patientDto.getWeight());
        patient.setTension(patientDto.getTension());
        patient.setTemperature(patientDto.getTemperature());
        patient.setDeleted(patientDto.isDeleted());

        return patient;
    }

    @Override
    public Patient fromFormToEntity(PatientForm patientForm) {
        Patient patient = new Patient();
        patient.setTemperature(patientForm.getTemperature());
        patient.setTension(patientForm.getTension());
        patient.setWeight(patientForm.getWeight());
        patient.setCode(Util.generateCode(PATIENT));
        patient.setName(patientForm.getName());
        patient.setSurname(patientForm.getSurname());
        patient.setDateBirth(patientForm.getDateBirth());
        patient.setPhone(patientForm.getPhone());
        patient.setSex(patientForm.getSex());

        return patient;
    }

    @Override
    public PatientDto fromEntityToDto(Patient patient) {
        PatientDto patientDto = new PatientDto();
        patientDto.setName(patient.getName());
        patientDto.setSurname(patient.getSurname());
        patientDto.setDateBirth(patient.getDateBirth());
        patientDto.setPhone(patientDto.getPhone());
        patientDto.setSex(patientDto.getSex());
        patientDto.setTemperature(patient.getTemperature());
        patientDto.setTension(patient.getTension());
        patientDto.setWeight(patient.getWeight());
        patientDto.setPatientCode(patient.getCode());
        patientDto.setDeleted(patient.isDeleted());

        return patientDto;
    }

    @Override
    public Patient deleteEntity(Patient patient) {
        patient.setDeleted(true);
        return patient;
    }
}
