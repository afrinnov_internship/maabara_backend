package com.afrinnov.maabara.patient;

import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.user.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Patient extends User {
    private float weight;
    private float temperature;
    private float tension;

    @OneToMany(mappedBy = "patient")
    @JsonIgnore
    private Set<Consultation> consultations= new HashSet<>();

    public Patient() {
    }

    public Set<Consultation> getConsultations() {
        return consultations;
    }

    public void setConsultations(Set<Consultation> consultations) {
        this.consultations = consultations;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getTension() {
        return tension;
    }

    public void setTension(float tension) {
        this.tension = tension;
    }
}
