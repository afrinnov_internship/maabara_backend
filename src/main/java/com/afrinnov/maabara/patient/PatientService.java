package com.afrinnov.maabara.patient;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.AbstractService;
import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService extends AbstractService<Patient, PatientDto, PatientForm> {

    private PatientRepository patientRepository;
    private PatientMapper patientMapper;

    public PatientService(PatientRepository patientRepository, PatientMapper patientMapper) {
        this.patientRepository = patientRepository;
        this.patientMapper = patientMapper;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<Patient> getRepository() {
        return this.patientRepository;
    }

    @Override
    protected AbstractMapper<Patient, PatientDto, PatientForm> getMapper() {
        return this.patientMapper;
    }

    @Override
    public PatientDto save(PatientForm patientForm) {
        return super.save(patientForm);
    }

    @Override
    public List<PatientDto> findAll() {
        return super.findAll();
    }

    @Override
    public PatientDto update(String code, PatientDto patientDto) {
        return super.update(code, patientDto);
    }

    @Override
    public void delete(String code) {
        super.delete(code);
    }

    @Override
    public PatientDto details(String code) {
        return super.details(code);
    }
}
