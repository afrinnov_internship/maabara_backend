package com.afrinnov.maabara.patient;

import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PatientRepository extends IRepository<Patient> {

}
