package com.afrinnov.maabara.patient;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/patient")
@CrossOrigin
public class PatientController {
    private PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/list")
    public List<PatientDto> listPatient(){
        return patientService.findAll();
    }

    @PostMapping(value = "/add")
    public PatientDto addPatient(@RequestBody PatientForm patientForm){
        return this.patientService.save(patientForm);
    }

    @PostMapping(value = "/modify")
    public PatientDto modifyPatient(@RequestBody PatientDto patientDto){
        return this.patientService.update(patientDto.getPatientCode(), patientDto);
    }

    @DeleteMapping(value = "/delete")
    public void deletePatient(@RequestParam String patientCode){
        this.patientService.delete(patientCode);
    }

    @PostMapping(value = "/details")
    public PatientDto detailsPatient(@RequestParam String patientCode){
        return this.patientService.details(patientCode);
    }
}
