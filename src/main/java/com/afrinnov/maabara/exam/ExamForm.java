package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.examgroup.ExamGroup;
import com.afrinnov.maabara.examgroup.ExamGroupDto;

import java.util.Date;

public class ExamForm {
    private String libelle;
    private Date dateAjout;
    private String description;
    private float minValue;
    private float maxValue;
    private float uniteResultat;
    private float prix;
    private String examGroup;

    public ExamForm() {
    }

    public String getExamGroup() {
        return examGroup;
    }

    public void setExamGroup(String examGroup) {
        this.examGroup = examGroup;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMinValue() {
        return minValue;
    }

    public void setMinValue(float minValue) {
        this.minValue = minValue;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }

    public float getUniteResultat() {
        return uniteResultat;
    }

    public void setUniteResultat(float uniteResultat) {
        this.uniteResultat = uniteResultat;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
