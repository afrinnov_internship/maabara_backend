package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.commons.AbstractEntity;
import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.examgroup.ExamGroup;
import com.afrinnov.maabara.subscribeExam.SubscribeExam;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Exam extends AbstractEntity {
    private String libelle;
    private Date dateAjout;
    private String description;
    private float minValue;
    private float maxValue;
    private float uniteResultat;
    private float prix;

    @ManyToOne
    @JoinColumn(name= "examGroup")
    private ExamGroup examGroup;

    @OneToMany(mappedBy = "exam")
    @JsonIgnore
    private Set<SubscribeExam> subscribeExams = new HashSet<>();

    public Exam() {
    }

    public Set<SubscribeExam> getSubscribeExams() {
        return subscribeExams;
    }

    public void setSubscribeExams(Set<SubscribeExam> subscribeExams) {
        this.subscribeExams = subscribeExams;
    }

    public ExamGroup getExamGroup() {
        return examGroup;
    }

    public void setExamGroup(ExamGroup examGroup) {
        this.examGroup = examGroup;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getMinValue() {
        return minValue;
    }

    public void setMinValue(float minValue) {
        this.minValue = minValue;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }

    public float getUniteResultat() {
        return uniteResultat;
    }

    public void setUniteResultat(float uniteResultat) {
        this.uniteResultat = uniteResultat;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
