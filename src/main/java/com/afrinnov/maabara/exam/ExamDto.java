package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.examgroup.ExamGroupDto;

import java.util.Date;

public class ExamDto {
    private String libelle;
    private Date dateAjout;
    private String description;
    private String examCode;
    private float minValue;
    private float maxValue;
    private float uniteResultat;
    private float prix;
    private boolean isDeleted;
    private ExamGroupDto examGroupDto;

    public boolean isDeleted() {
        return isDeleted;
    }

    public ExamGroupDto getExamGroupDto() {
        return examGroupDto;
    }

    public void setExamGroupDto(ExamGroupDto examGroupDto) {
        this.examGroupDto = examGroupDto;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public float getMinValue() {
        return minValue;
    }

    public void setMinValue(float minValue) {
        this.minValue = minValue;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }

    public float getUniteResultat() {
        return uniteResultat;
    }

    public void setUniteResultat(float uniteResultat) {
        this.uniteResultat = uniteResultat;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
}
