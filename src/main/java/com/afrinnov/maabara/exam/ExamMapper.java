package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.examgroup.ExamGroup;
import com.afrinnov.maabara.examgroup.ExamGroupMapper;
import com.afrinnov.maabara.examgroup.ExamGroupRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.afrinnov.maabara.commons.PrefixCode.EXAM;

@Component
public class ExamMapper extends AbstractMapper<Exam, ExamDto, ExamForm> {
    private ExamGroupMapper examGroupMapper;
    private ExamGroupRepository examGroupRepository;

    public ExamMapper(ExamGroupMapper examGroupMapper, ExamGroupRepository examGroupRepository) {
        this.examGroupMapper = examGroupMapper;
        this.examGroupRepository = examGroupRepository;
    }

    @Override
    public Exam fromDtoToEntity(ExamDto examDto) {
        Exam exam = new Exam();
        exam.setDateAjout(examDto.getDateAjout());
        exam.setDescription(examDto.getDescription());
        exam.setLibelle(examDto.getLibelle());
        exam.setMaxValue(examDto.getMaxValue());
        exam.setMinValue(examDto.getMinValue());
        exam.setCode(examDto.getExamCode());
        exam.setUniteResultat(examDto.getUniteResultat());
        exam.setPrix(examDto.getPrix());
        exam.setDeleted(examDto.isDeleted());
        exam.setExamGroup(this.examGroupMapper.fromDtoToEntity(examDto.getExamGroupDto()));

        return exam;
    }

    @Override
    public Exam CopyDtoToEntity(Exam exam, ExamDto examDto) {
        exam.setDateAjout(examDto.getDateAjout());
        exam.setDescription(examDto.getDescription());
        exam.setLibelle(examDto.getLibelle());
        exam.setMaxValue(examDto.getMaxValue());
        exam.setMinValue(examDto.getMinValue());
        exam.setCode(examDto.getExamCode());
        exam.setUniteResultat(examDto.getUniteResultat());
        exam.setPrix(examDto.getPrix());
        exam.setExamGroup(this.examGroupMapper.fromDtoToEntity(examDto.getExamGroupDto()));

        return exam;
    }

    @Override
    public Exam fromFormToEntity(ExamForm examForm) {
        Exam exam = new Exam();
        Optional<ExamGroup> examGroup_opt = this.examGroupRepository.findByCode(examForm.getExamGroup());
        ExamGroup examGroup = examGroup_opt.get();
        exam.setExamGroup(examGroup);
        exam.setDateAjout(examForm.getDateAjout());
        exam.setDescription(examForm.getDescription());
        exam.setLibelle(examForm.getLibelle());
        exam.setMaxValue(examForm.getMaxValue());
        exam.setMinValue(examForm.getMinValue());
        exam.setUniteResultat(examForm.getUniteResultat());
        exam.setPrix(examForm.getPrix());
        exam.setCode(Util.generateCode(EXAM));

        return exam;
    }

    @Override
    public ExamDto fromEntityToDto(Exam exam) {
        ExamDto examDto = new ExamDto();
        examDto.setDateAjout(exam.getDateAjout());
        examDto.setDescription(exam.getDescription());
        examDto.setLibelle(exam.getLibelle());
        examDto.setMaxValue(exam.getMaxValue());
        examDto.setMinValue(exam.getMinValue());
        examDto.setExamCode(exam.getCode());
        examDto.setUniteResultat(exam.getUniteResultat());
        examDto.setPrix(exam.getPrix());
        examDto.setDeleted(exam.isDeleted());
        examDto.setExamGroupDto(this.examGroupMapper.fromEntityToDto(exam.getExamGroup()));

        return examDto;
    }

    @Override
    public Exam deleteEntity(Exam exam) {
        exam.setDeleted(true);
        return exam;
    }
}
