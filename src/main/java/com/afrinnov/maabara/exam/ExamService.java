package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.AbstractService;
import com.afrinnov.maabara.commons.IRepository;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.examgroup.ExamGroup;
import com.afrinnov.maabara.examgroup.ExamGroupDto;
import com.afrinnov.maabara.examgroup.ExamGroupMapper;
import com.afrinnov.maabara.examgroup.ExamGroupRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.afrinnov.maabara.commons.PrefixCode.EXAM;

@Service
public class ExamService extends AbstractService<Exam, ExamDto, ExamForm> {
    private ExamRepository examRepository;
    private ExamMapper examMapper;
    private ExamGroupRepository examGroupRepository;
    private ExamGroupMapper examGroupMapper;

    public ExamService(ExamRepository examRepository, ExamMapper examMapper, ExamGroupRepository examGroupRepository, ExamGroupMapper examGroupMapper) {
        this.examRepository = examRepository;
        this.examMapper = examMapper;
        this.examGroupRepository = examGroupRepository;
        this.examGroupMapper = examGroupMapper;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<Exam> getRepository() {
        return this.examRepository;
    }

    @Override
    protected AbstractMapper<Exam, ExamDto, ExamForm> getMapper() {
        return this.examMapper;
    }

    @Override
    public ExamDto save(ExamForm examForm) {
        Exam exam = new Exam();
        Optional<ExamGroup> examGroup_opt = this.examGroupRepository.findByCode(examForm.getExamGroup());
        ExamGroup examGroup = examGroup_opt.get();
        exam.setExamGroup(examGroup);
        exam.setDateAjout(examForm.getDateAjout());
        exam.setDescription(examForm.getDescription());
        exam.setLibelle(examForm.getLibelle());
        exam.setMaxValue(examForm.getMaxValue());
        exam.setMinValue(examForm.getMinValue());
        exam.setUniteResultat(examForm.getUniteResultat());
        exam.setPrix(examForm.getPrix());
        exam.setCode(Util.generateCode(EXAM));
        exam = this.examRepository.save(exam);

        ExamDto examDto = new ExamDto();
        examDto.setDateAjout(exam.getDateAjout());
        examDto.setDescription(exam.getDescription());
        examDto.setLibelle(exam.getLibelle());
        examDto.setMaxValue(exam.getMaxValue());
        examDto.setMinValue(exam.getMinValue());
        examDto.setExamCode(exam.getCode());
        examDto.setUniteResultat(exam.getUniteResultat());
        examDto.setPrix(exam.getPrix());
        examDto.setDeleted(exam.isDeleted());
        examDto.setExamGroupDto(this.examGroupMapper.fromEntityToDto(examGroup));

        return examDto;
    }

    @Override
    public List<ExamDto> findAll() {
        List<Exam> exams = this.examRepository.findAll();
        List<ExamDto> examsDto = new ArrayList<>();
        for (int i=0; i<exams.size();i++){
            ExamDto examDto = new ExamDto();
            examDto.setDateAjout(exams.get(i).getDateAjout());
            examDto.setDescription(exams.get(i).getDescription());
            examDto.setLibelle(exams.get(i).getLibelle());
            examDto.setMaxValue(exams.get(i).getMaxValue());
            examDto.setMinValue(exams.get(i).getMinValue());
            examDto.setExamCode(exams.get(i).getCode());
            examDto.setUniteResultat(exams.get(i).getUniteResultat());
            examDto.setPrix(exams.get(i).getPrix());
            examDto.setDeleted(exams.get(i).isDeleted());
            Optional<ExamGroup> examGroup_opt = this.examGroupRepository.findByCode(exams.get(i).getExamGroup().getCode());
            if (examGroup_opt.isPresent()){
                ExamGroup examGroup = examGroup_opt.get();
                ExamGroupDto examGroupDto = this.examGroupMapper.fromEntityToDto(examGroup);
                examDto.setExamGroupDto(examGroupDto);
            }

            examsDto.add(examDto);
        }

        return examsDto;
    }

    @Override
    public ExamDto findByCode(String code) {
        return super.findByCode(code);
    }

    @Override
    public ExamDto update(String code, ExamDto examDto) {
        Optional<Exam> exam_opt = this.examRepository.findByCode(code);
        if (exam_opt.isPresent()){
            Exam exam = exam_opt.get();
            Optional<ExamGroup> examGroup_opt = this.examGroupRepository.findByCode(examDto.getExamGroupDto().getExamGroupCode());
            if (examGroup_opt.isPresent()){
                ExamGroup examGroup = examGroup_opt.get();

                exam.setDateAjout(examDto.getDateAjout());
                exam.setDescription(examDto.getDescription());
                exam.setLibelle(examDto.getLibelle());
                exam.setMaxValue(examDto.getMaxValue());
                exam.setMinValue(examDto.getMinValue());
                exam.setCode(examDto.getExamCode());
                exam.setUniteResultat(examDto.getUniteResultat());
                exam.setPrix(examDto.getPrix());
                exam.setExamGroup(examGroup);
                exam = this.examRepository.save(exam);

                ExamDto examDto1 = new ExamDto();
                examDto1.setDateAjout(exam.getDateAjout());
                examDto1.setDescription(exam.getDescription());
                examDto1.setLibelle(exam.getLibelle());
                examDto1.setMaxValue(exam.getMaxValue());
                examDto1.setMinValue(exam.getMinValue());
                examDto1.setExamCode(exam.getCode());
                examDto1.setUniteResultat(exam.getUniteResultat());
                examDto1.setPrix(exam.getPrix());
                examDto1.setDeleted(exam.isDeleted());
                examDto1.setExamGroupDto(examDto.getExamGroupDto());

                return examDto1;
            }
        }
        return null;
    }

    @Override
    public void delete(String code) {
        super.delete(code);
    }

    @Override
    public ExamDto details(String code) {
        Optional<Exam> exam_opt = this.examRepository.findByCode(code);
        if (exam_opt.isPresent()){
            Exam exam = exam_opt.get();
            ExamDto examDto1 = new ExamDto();
            examDto1.setDateAjout(exam.getDateAjout());
            examDto1.setDescription(exam.getDescription());
            examDto1.setLibelle(exam.getLibelle());
            examDto1.setMaxValue(exam.getMaxValue());
            examDto1.setMinValue(exam.getMinValue());
            examDto1.setExamCode(exam.getCode());
            examDto1.setUniteResultat(exam.getUniteResultat());
            examDto1.setPrix(exam.getPrix());
            examDto1.setDeleted(exam.isDeleted());
            ExamGroupDto examGroupDto = this.examGroupMapper.fromEntityToDto(exam.getExamGroup());
            examDto1.setExamGroupDto(examGroupDto);

            return examDto1;
        }

        return null;
    }
}
