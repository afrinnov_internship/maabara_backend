package com.afrinnov.maabara.exam;

import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamRepository extends IRepository<Exam> {

}
