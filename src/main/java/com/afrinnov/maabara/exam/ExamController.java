package com.afrinnov.maabara.exam;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/exam")
@CrossOrigin
public class ExamController {
    private ExamService examService;

    public ExamController(ExamService examService) {
        this.examService = examService;
    }

    @GetMapping("/list")
    public List<ExamDto> listExam(){
        return this.examService.findAll();
    }

    @PostMapping(value = "/add")
    public ExamDto addExam(@RequestBody ExamForm examForm){
        return this.examService.save(examForm);
    }

    @PostMapping(value = "/modify")
    public ExamDto modifyExam(@RequestBody ExamDto examDto){
        return this.examService.update(examDto.getExamCode(), examDto);
    }

    @DeleteMapping(value = "/delete")
    public void deleteExam(@RequestParam String examCode){
        this.examService.delete(examCode);
    }

    @PostMapping(value = "/details")
    public ExamDto detailsExam(@RequestParam String examCode){
        return this.examService.details(examCode);
    }
}
