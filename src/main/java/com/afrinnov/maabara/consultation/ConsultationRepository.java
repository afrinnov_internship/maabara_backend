package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsultationRepository extends IRepository<Consultation> {


}
