package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.AbstractService;
import com.afrinnov.maabara.commons.IRepository;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.patient.Patient;
import com.afrinnov.maabara.patient.PatientDto;
import com.afrinnov.maabara.patient.PatientMapper;
import com.afrinnov.maabara.patient.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.afrinnov.maabara.commons.PrefixCode.CONSULTATION;

@Service
public class ConsultationService extends AbstractService<Consultation, ConsultationDto, ConsultationForm> {


    private ConsultationRepository consultationRepository;
    private ConsultationMapper consultationMapper;
    private PatientMapper patientMapper;
    private PatientRepository patientRepository;

    public ConsultationService(ConsultationRepository consultationRepository, ConsultationMapper consultationMapper, PatientMapper patientMapper, PatientRepository patientRepository) {
        this.consultationRepository = consultationRepository;
        this.consultationMapper = consultationMapper;
        this.patientMapper = patientMapper;
        this.patientRepository = patientRepository;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<Consultation> getRepository() {
        return this.consultationRepository;
    }

    @Override
    protected AbstractMapper<Consultation, ConsultationDto, ConsultationForm> getMapper() {
        return this.consultationMapper;
    }

    @Override
    public ConsultationDto save(ConsultationForm consultationForm) {
        Consultation consultation = new Consultation();
        Optional<Patient> patient_opt = this.patientRepository.findByCode(consultationForm.getPatient());
        Patient patient = patient_opt.get();
        consultation.setPatient(patient);
        consultation.setConsultationDoctor(consultationForm.getConsultationDoctor());
        consultation.setCode(Util.generateCode(CONSULTATION));
        consultation.setConsultationDateConsultation(consultationForm.getConsultationDateConsultation());
        consultation.setConsultationStatus(consultationForm.getConsultationStatus());
        consultation = this.consultationRepository.save(consultation);

        ConsultationDto consultationDto1 = new ConsultationDto();
        consultationDto1.setConsultationDoctor(consultation.getConsultationDoctor());
        consultationDto1.setConsultationDateConsultation(consultation.getConsultationDateConsultation());
        consultationDto1.setConsultationStatus(consultation.getConsultationStatus());
        consultationDto1.setConsultationCode(consultation.getCode());
        consultationDto1.setDeleted(consultation.isDeleted());
        PatientDto patientDto = this.patientMapper.fromEntityToDto(patient);
        consultationDto1.setPatient(patientDto);
        return consultationDto1;
    }

    @Override
    public List<ConsultationDto> findAll() {
        List<Consultation> consultations = this.consultationRepository.findAll();
        List<ConsultationDto> consultationsDto = new ArrayList<>();
        for (int i=0; i<consultations.size();i++){
            ConsultationDto consultationDto1 = new ConsultationDto();
            consultationDto1.setConsultationDoctor(consultations.get(i).getConsultationDoctor());
            consultationDto1.setConsultationDateConsultation(consultations.get(i).getConsultationDateConsultation());
            consultationDto1.setConsultationStatus(consultations.get(i).getConsultationStatus());
            consultationDto1.setConsultationCode(consultations.get(i).getCode());
            consultationDto1.setDeleted(consultations.get(i).isDeleted());
            Optional<Patient> patient_opt = this.patientRepository.findByCode(consultations.get(i).getPatient().getCode());
            Patient patient = patient_opt.get();
            PatientDto patientDto = this.patientMapper.fromEntityToDto(patient);
            consultationDto1.setPatient(patientDto);
            consultationsDto.add(consultationDto1);
        }

        return consultationsDto;
    }

    @Override
    public ConsultationDto update(String code, ConsultationDto consultationDto) {
        Optional<Consultation> consultation_opt = this.consultationRepository.findByCode(code);
        if (consultation_opt.isPresent()){
            Consultation consultation = consultation_opt.get();
            Optional<Patient> patient_opt = this.patientRepository.findByCode(consultationDto.getPatient().getPatientCode());
            if (patient_opt.isPresent()){
                Patient patient = patient_opt.get();
                consultation.setConsultationDoctor(consultationDto.getConsultationDoctor());
                consultation.setConsultationDateConsultation(consultationDto.getConsultationDateConsultation());
                consultation.setConsultationStatus(consultationDto.getConsultationStatus());
                consultation.setPatient(patient);
                consultation = this.consultationRepository.save(consultation);

                ConsultationDto consultationDto1 = new ConsultationDto();
                consultationDto1.setConsultationDoctor(consultation.getConsultationDoctor());
                consultationDto1.setConsultationDateConsultation(consultation.getConsultationDateConsultation());
                consultationDto1.setConsultationStatus(consultation.getConsultationStatus());
                consultationDto1.setConsultationCode(consultation.getCode());
                consultationDto1.setDeleted(consultation.isDeleted());
                PatientDto patientDto = this.patientMapper.fromEntityToDto(patient);
                consultationDto1.setPatient(patientDto);
                return consultationDto1;
            }
        }

        return null;
    }

    @Override
    public void delete(String code) {
        super.delete(code);
    }

    @Override
    public ConsultationDto details(String code) {
        Optional<Consultation> consultation_opt = this.consultationRepository.findByCode(code);
        if (consultation_opt.isPresent()){
            Consultation consultation = consultation_opt.get();
            PatientDto patientDto = this.patientMapper.fromEntityToDto(consultation.getPatient());
            ConsultationDto consultationDto = new ConsultationDto();
            consultationDto.setConsultationDoctor(consultation.getConsultationDoctor());
            consultationDto.setConsultationDateConsultation(consultation.getConsultationDateConsultation());
            consultationDto.setConsultationStatus(consultation.getConsultationStatus());
            consultationDto.setConsultationCode(consultation.getCode());
            consultationDto.setPatient(patientDto);
            consultationDto.setDeleted(consultation.isDeleted());
            return consultationDto;
        }

        return null;
    }
}
