package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.exam.ExamDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/consultation")
@CrossOrigin
public class ConsultationController {
    private ConsultationService consultationService;

    public ConsultationController(ConsultationService consultationService) {
        this.consultationService = consultationService;
    }

    @GetMapping("/list")
    public List<ConsultationDto> listConsultation(){
        return consultationService.findAll();
    }

    @PostMapping(value = "/add")
    public ConsultationDto addConsultation(@RequestBody ConsultationForm consultationForm){
        return this.consultationService.save(consultationForm);
    }

    @PostMapping(value = "/modify")
    public ConsultationDto modifyConsultation(@RequestBody ConsultationDto consultationDto){
        return this.consultationService.update(consultationDto.getConsultationCode(), consultationDto);
    }

    @DeleteMapping(value = "/delete")
    public void deleteConsultation(@RequestParam String consultationCode){
        this.consultationService.delete(consultationCode);
    }

    @PostMapping(value = "/details")
    public ConsultationDto detailsConsultation(@RequestParam String consultationCode){
        return this.consultationService.details(consultationCode);
    }

}
