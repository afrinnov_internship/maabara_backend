package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.patient.PatientDto;

import java.util.Date;

public class ConsultationForm {
    private String consultationDoctor;
    private Date consultationDateConsultation;
    private String consultationStatus;
    private String patient;

    public ConsultationForm() {
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public String getConsultationStatus() {
        return consultationStatus;
    }

    public void setConsultationStatus(String consultationStatus) {
        this.consultationStatus = consultationStatus;
    }

    public String getConsultationDoctor() {
        return consultationDoctor;
    }

    public void setConsultationDoctor(String consultationDoctor) {
        this.consultationDoctor = consultationDoctor;
    }

    public Date getConsultationDateConsultation() {
        return consultationDateConsultation;
    }

    public void setConsultationDateConsultation(Date consultationDateConsultation) {
        this.consultationDateConsultation = consultationDateConsultation;
    }
}
