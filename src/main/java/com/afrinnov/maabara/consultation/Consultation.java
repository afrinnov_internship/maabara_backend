package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.commons.AbstractEntity;
import com.afrinnov.maabara.exam.Exam;
import com.afrinnov.maabara.patient.Patient;
import com.afrinnov.maabara.patient.PatientDto;
import com.afrinnov.maabara.subscribeExam.SubscribeExam;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Consultation extends AbstractEntity {
    private String consultationDoctor;
    private Date consultationDateConsultation;
    private String consultationStatus;

    @ManyToOne
    @JoinColumn(name= "patient")
    private Patient patient;

    @OneToMany(mappedBy = "consultation")
    @JsonIgnore
    private Set<SubscribeExam> subscribeExams = new HashSet<>();

    public Consultation() {
    }

    public Set<SubscribeExam> getSubscribeExams() {
        return subscribeExams;
    }

    public void setSubscribeExams(Set<SubscribeExam> subscribeExams) {
        this.subscribeExams = subscribeExams;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getConsultationDoctor() {
        return consultationDoctor;
    }

    public void setConsultationDoctor(String consultationDoctor) {
        this.consultationDoctor = consultationDoctor;
    }

    public Date getConsultationDateConsultation() {
        return consultationDateConsultation;
    }

    public void setConsultationDateConsultation(Date consultationDateConsultation) {
        this.consultationDateConsultation = consultationDateConsultation;
    }

    public String getConsultationStatus() {
        return consultationStatus;
    }

    public void setConsultationStatus(String consultationStatus) {
        this.consultationStatus = consultationStatus;
    }
}
