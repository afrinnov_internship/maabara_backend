package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.exam.ExamDto;
import com.afrinnov.maabara.patient.PatientDto;

import java.util.Date;
import java.util.List;

public class ConsultationDto {
    private String consultationDoctor;
    private Date consultationDateConsultation;
    private String consultationStatus;
    private String consultationCode;
    private PatientDto patient;
    private boolean isDeleted;

    public ConsultationDto() {
    }

    public PatientDto getPatient() {
        return patient;
    }

    public void setPatient(PatientDto patient) {
        this.patient = patient;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getConsultationCode() {
        return consultationCode;
    }

    public void setConsultationCode(String consultationCode) {
        this.consultationCode = consultationCode;
    }

    public String getConsultationDoctor() {
        return consultationDoctor;
    }

    public void setConsultationDoctor(String consultationDoctor) {
        this.consultationDoctor = consultationDoctor;
    }

    public Date getConsultationDateConsultation() {
        return consultationDateConsultation;
    }

    public void setConsultationDateConsultation(Date consultationDateConsultation) {
        this.consultationDateConsultation = consultationDateConsultation;
    }

    public String getConsultationStatus() {
        return consultationStatus;
    }

    public void setConsultationStatus(String consultationStatus) {
        this.consultationStatus = consultationStatus;
    }
}
