package com.afrinnov.maabara.consultation;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.patient.Patient;
import com.afrinnov.maabara.patient.PatientDto;
import com.afrinnov.maabara.patient.PatientMapper;
import com.afrinnov.maabara.patient.PatientRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.afrinnov.maabara.commons.PrefixCode.CONSULTATION;

@Component
public class ConsultationMapper extends AbstractMapper<Consultation, ConsultationDto, ConsultationForm> {
    private PatientRepository patientRepository;
    private PatientMapper patientMapper;

    public ConsultationMapper(PatientRepository patientRepository, PatientMapper patientMapper) {
        this.patientRepository = patientRepository;
        this.patientMapper = patientMapper;
    }

    @Override
    public Consultation CopyDtoToEntity(Consultation consultation, ConsultationDto consultationDto) {
        consultation.setConsultationDoctor(consultationDto.getConsultationDoctor());
        consultation.setConsultationDateConsultation(consultationDto.getConsultationDateConsultation());
        consultation.setConsultationStatus(consultationDto.getConsultationStatus());
        consultation.setPatient(this.patientMapper.fromDtoToEntity(consultationDto.getPatient()));

        return consultation;
    }

    @Override
    public Consultation fromDtoToEntity(ConsultationDto consultationDto) {
        Consultation consultation = new Consultation();
        consultation.setConsultationDoctor(consultationDto.getConsultationDoctor());
        consultation.setConsultationDateConsultation(consultationDto.getConsultationDateConsultation());
        consultation.setConsultationStatus(consultationDto.getConsultationStatus());
        consultation.setPatient(this.patientMapper.fromDtoToEntity(consultationDto.getPatient()));
        consultation.setCode(consultationDto.getConsultationCode());
        consultation.setDeleted(consultationDto.isDeleted());
        return consultation;
    }

    @Override
    public Consultation fromFormToEntity(ConsultationForm consultationForm) {
        Consultation consultation = new Consultation();
        Optional<Patient> patient_opt = this.patientRepository.findByCode(consultationForm.getPatient());
        Patient patient = patient_opt.get();
        consultation.setPatient(patient);
        consultation.setConsultationDoctor(consultationForm.getConsultationDoctor());
        consultation.setCode(Util.generateCode(CONSULTATION));
        consultation.setConsultationDateConsultation(consultationForm.getConsultationDateConsultation());
        consultation.setConsultationStatus(consultationForm.getConsultationStatus());

        return consultation;
    }

    @Override
    public ConsultationDto fromEntityToDto(Consultation consultation) {
        ConsultationDto consultationDto = new ConsultationDto();
        consultationDto.setConsultationDoctor(consultation.getConsultationDoctor());
        consultationDto.setConsultationDateConsultation(consultation.getConsultationDateConsultation());
        consultationDto.setConsultationStatus(consultation.getConsultationStatus());
        consultationDto.setConsultationCode(consultation.getCode());
        consultationDto.setDeleted(consultation.isDeleted());
        PatientDto patientDto = this.patientMapper.fromEntityToDto(consultation.getPatient());
        consultationDto.setPatient(patientDto);

        return consultationDto;
    }

    @Override
    public Consultation deleteEntity(Consultation consultation) {
        consultation.setDeleted(true);
        return consultation;
    }
}
