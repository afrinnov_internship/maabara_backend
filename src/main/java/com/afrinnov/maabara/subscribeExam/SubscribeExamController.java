package com.afrinnov.maabara.subscribeExam;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/subscribeExam")
@CrossOrigin
public class SubscribeExamController {
    private SubscribeExamService subscribeExamService;

    public SubscribeExamController(SubscribeExamService subscribeExamService) {
        this.subscribeExamService = subscribeExamService;
    }

    @GetMapping("/list")
    public List<SubscribeExamDto> listSubscribeExam(){
        return subscribeExamService.findAll();
    }

    @PostMapping(value = "/add")
    public SubscribeExamDto addSubscribeExam(@RequestBody SubscribeExamForm subscribeExamForm){
        return this.subscribeExamService.save(subscribeExamForm);
    }

    @PostMapping(value = "/modify")
    public SubscribeExamDto modifySubscribeExam(@RequestBody SubscribeExamDto subscribeExamDto){
        return this.subscribeExamService.update(subscribeExamDto.getSubscribeExamCode(), subscribeExamDto);
    }

    @DeleteMapping(value = "/delete")
    public void deleteSubscribeExam(@RequestParam String subscribeExamCode){
        this.subscribeExamService.delete(subscribeExamCode);
    }

    @PostMapping(value = "/details")
    public SubscribeExamDto detailsSubscribeExam(@RequestParam String subscribeExamCode){
        return this.subscribeExamService.details(subscribeExamCode);
    }
}
