package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.commons.AbstractEntity;
import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.exam.Exam;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class SubscribeExam extends AbstractEntity {
    private Date date;
    private float result;

    @ManyToOne
    @JoinColumn(name= "exam")
    private Exam exam;

    @ManyToOne
    @JoinColumn(name= "consultation")
    private Consultation consultation;

    public SubscribeExam() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Consultation getConsultation() {
        return consultation;
    }

    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }
}
