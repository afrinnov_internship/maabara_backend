package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.consultation.ConsultationDto;
import com.afrinnov.maabara.exam.ExamDto;

import java.util.Date;

public class SubscribeExamForm {
    private Date date;
    private float result;
    private String examCode;
    private String consultationCode;

    public SubscribeExamForm() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public String getConsultationCode() {
        return consultationCode;
    }

    public void setConsultationCode(String consultationCode) {
        this.consultationCode = consultationCode;
    }
}
