package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.AbstractService;
import com.afrinnov.maabara.commons.IRepository;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.consultation.ConsultationMapper;
import com.afrinnov.maabara.consultation.ConsultationRepository;
import com.afrinnov.maabara.exam.Exam;
import com.afrinnov.maabara.exam.ExamMapper;
import com.afrinnov.maabara.exam.ExamRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.afrinnov.maabara.commons.PrefixCode.SOUSCRITEXAM;

@Service
public class SubscribeExamService extends AbstractService<SubscribeExam, SubscribeExamDto, SubscribeExamForm> {
    private SubscribeExamMapper subscribeExamMapper;
    private SubscribeExamRepository subscribeExamRepository;
    private ExamRepository examRepository;
    private ExamMapper examMapper;
    private ConsultationRepository consultationRepository;
    private ConsultationMapper consultationMapper;

    public SubscribeExamService(SubscribeExamMapper subscribeExamMapper, SubscribeExamRepository subscribeExamRepository, ExamRepository examRepository, ExamMapper examMapper, ConsultationRepository consultationRepository, ConsultationMapper consultationMapper) {
        this.subscribeExamMapper = subscribeExamMapper;
        this.subscribeExamRepository = subscribeExamRepository;
        this.examRepository = examRepository;
        this.examMapper = examMapper;
        this.consultationRepository = consultationRepository;
        this.consultationMapper = consultationMapper;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<SubscribeExam> getRepository() {
        return this.subscribeExamRepository;
    }

    @Override
    protected AbstractMapper<SubscribeExam, SubscribeExamDto, SubscribeExamForm> getMapper() {
        return this.subscribeExamMapper;
    }

    @Override
    public SubscribeExamDto save(SubscribeExamForm subscribeExamForm) {
        Optional<Consultation> consultation_opt = this.consultationRepository.findByCode(subscribeExamForm.getConsultationCode());
        Optional<Exam> exam_opt = this.examRepository.findByCode(subscribeExamForm.getExamCode());
        if (consultation_opt.isPresent() && exam_opt.isPresent()){
            Consultation consultation = consultation_opt.get();
            Exam exam = exam_opt.get();
            SubscribeExam subscribeExam = new SubscribeExam();
            subscribeExam.setConsultation(consultation);
            subscribeExam.setExam(exam);
            subscribeExam.setDate(subscribeExamForm.getDate());
            subscribeExam.setResult(subscribeExamForm.getResult());
            subscribeExam.setCode(Util.generateCode(SOUSCRITEXAM));
            subscribeExam = this.subscribeExamRepository.save(subscribeExam);

            SubscribeExamDto subscribeExamDto = new SubscribeExamDto();
            subscribeExamDto.setDate(subscribeExam.getDate());
            subscribeExamDto.setResult(subscribeExam.getResult());
            subscribeExamDto.setConsultationDto(this.consultationMapper.fromEntityToDto(consultation));
            subscribeExamDto.setExamDto(this.examMapper.fromEntityToDto(exam));
            subscribeExamDto.setDeleted(subscribeExam.isDeleted());
            subscribeExamDto.setSubscribeExamCode(subscribeExam.getCode());

            return subscribeExamDto;
        }
        return null;
    }

    @Override
    public List<SubscribeExamDto> findAll() {
        List<SubscribeExam> subscribeExams = this.subscribeExamRepository.findAll();
        List<SubscribeExamDto> subscribeExamsDto = new ArrayList<>();
        for (int i=0; i<subscribeExams.size();i++){
            SubscribeExamDto subscribeExamDto = new SubscribeExamDto();
            subscribeExamDto.setSubscribeExamCode(subscribeExams.get(i).getCode());
            subscribeExamDto.setDeleted(subscribeExams.get(i).isDeleted());
            subscribeExamDto.setResult(subscribeExams.get(i).getResult());
            subscribeExamDto.setDate(subscribeExams.get(i).getDate());
            subscribeExamDto.setExamDto(this.examMapper.fromEntityToDto(subscribeExams.get(i).getExam()));
            subscribeExamDto.setConsultationDto(this.consultationMapper.fromEntityToDto(subscribeExams.get(i).getConsultation()));
            subscribeExamsDto.add(subscribeExamDto);
        }

        return subscribeExamsDto;
    }

    @Override
    public SubscribeExamDto findByCode(String code) {
        return super.findByCode(code);
    }

    @Override
    public SubscribeExamDto update(String code, SubscribeExamDto subscribeExamDto) {
        Optional<SubscribeExam> subscribeExam_opt = this.subscribeExamRepository.findByCode(code);
        if (subscribeExam_opt.isPresent()){
            SubscribeExam subscribeExam = subscribeExam_opt.get();

        }

        return null;
    }

    @Override
    public void delete(String code) {
        super.delete(code);
    }

    @Override
    public SubscribeExamDto details(String code) {
        Optional<SubscribeExam> subscribeExam_opt = this.subscribeExamRepository.findByCode(code);
        if (subscribeExam_opt.isPresent()){
            SubscribeExam subscribeExam = subscribeExam_opt.get();
            SubscribeExamDto subscribeExamDto = new SubscribeExamDto();
            subscribeExamDto.setSubscribeExamCode(subscribeExam.getCode());
            subscribeExamDto.setDeleted(subscribeExam.isDeleted());
            subscribeExamDto.setResult(subscribeExam.getResult());
            subscribeExamDto.setDate(subscribeExam.getDate());
            subscribeExamDto.setExamDto(this.examMapper.fromEntityToDto(subscribeExam.getExam()));
            subscribeExamDto.setConsultationDto(this.consultationMapper.fromEntityToDto(subscribeExam.getConsultation()));

            return subscribeExamDto;
        }

        return null;
    }
}
