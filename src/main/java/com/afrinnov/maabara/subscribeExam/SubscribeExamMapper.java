package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.Util;
import com.afrinnov.maabara.consultation.Consultation;
import com.afrinnov.maabara.consultation.ConsultationMapper;
import com.afrinnov.maabara.consultation.ConsultationRepository;
import com.afrinnov.maabara.exam.Exam;
import com.afrinnov.maabara.exam.ExamMapper;
import com.afrinnov.maabara.exam.ExamRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.afrinnov.maabara.commons.PrefixCode.SOUSCRITEXAM;

@Component
public class SubscribeExamMapper extends AbstractMapper<SubscribeExam, SubscribeExamDto, SubscribeExamForm> {
    private ExamRepository examRepository;
    private ExamMapper examMapper;
    private ConsultationRepository consultationRepository;
    private ConsultationMapper consultationMapper;

    public SubscribeExamMapper(ExamRepository examRepository, ExamMapper examMapper, ConsultationRepository consultationRepository, ConsultationMapper consultationMapper) {
        this.examRepository = examRepository;
        this.examMapper = examMapper;
        this.consultationRepository = consultationRepository;
        this.consultationMapper = consultationMapper;
    }


    @Override
    public SubscribeExam fromDtoToEntity(SubscribeExamDto subscribeExamDto) {

        return null;
    }

    @Override
    public SubscribeExam CopyDtoToEntity(SubscribeExam subscribeExam, SubscribeExamDto subscribeExamDto) {
        return null;
    }

    @Override
    public SubscribeExam fromFormToEntity(SubscribeExamForm subscribeExamForm) {
        Optional<Consultation> consultation_opt = this.consultationRepository.findByCode(subscribeExamForm.getConsultationCode());
        Optional<Exam> exam_opt = this.examRepository.findByCode(subscribeExamForm.getExamCode());
        if (consultation_opt.isPresent() && exam_opt.isPresent()) {
            Consultation consultation = consultation_opt.get();
            Exam exam = exam_opt.get();
            SubscribeExam subscribeExam = new SubscribeExam();
            subscribeExam.setConsultation(consultation);
            subscribeExam.setExam(exam);
            subscribeExam.setDate(subscribeExamForm.getDate());
            subscribeExam.setResult(subscribeExamForm.getResult());
            subscribeExam.setCode(Util.generateCode(SOUSCRITEXAM));

            return subscribeExam;
        }
        return null;
    }

    @Override
    public SubscribeExamDto fromEntityToDto(SubscribeExam subscribeExam) {
        SubscribeExamDto subscribeExamDto = new SubscribeExamDto();
        subscribeExamDto.setDate(subscribeExam.getDate());
        subscribeExamDto.setResult(subscribeExam.getResult());
        subscribeExamDto.setConsultationDto(this.consultationMapper.fromEntityToDto(subscribeExam.getConsultation()));
        subscribeExamDto.setExamDto(this.examMapper.fromEntityToDto(subscribeExam.getExam()));
        subscribeExamDto.setDeleted(subscribeExam.isDeleted());
        subscribeExamDto.setSubscribeExamCode(subscribeExam.getCode());

        return subscribeExamDto;
    }

    @Override
    public SubscribeExam deleteEntity(SubscribeExam subscribeExam) {
        subscribeExam.setDeleted(true);
        return subscribeExam;
    }
}
