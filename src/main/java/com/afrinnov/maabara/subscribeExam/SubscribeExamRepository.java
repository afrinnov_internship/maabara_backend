package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscribeExamRepository extends IRepository<SubscribeExam> {
}
