package com.afrinnov.maabara.subscribeExam;

import com.afrinnov.maabara.consultation.ConsultationDto;
import com.afrinnov.maabara.exam.ExamDto;

import java.util.Date;

public class SubscribeExamDto {
    private Date date;
    private float result;
    private String subscribeExamCode;
    private boolean isDeleted;
    private ExamDto examDto;
    private ConsultationDto consultationDto;

    public SubscribeExamDto() {
    }

    public String getSubscribeExamCode() {
        return subscribeExamCode;
    }

    public void setSubscribeExamCode(String subscribeExamCode) {
        this.subscribeExamCode = subscribeExamCode;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public ExamDto getExamDto() {
        return examDto;
    }

    public void setExamDto(ExamDto examDto) {
        this.examDto = examDto;
    }

    public ConsultationDto getConsultationDto() {
        return consultationDto;
    }

    public void setConsultationDto(ConsultationDto consultationDto) {
        this.consultationDto = consultationDto;
    }
}
