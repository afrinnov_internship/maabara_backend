package com.afrinnov.maabara.examgroup;

import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamGroupRepository extends IRepository<ExamGroup> {

}
