package com.afrinnov.maabara.examgroup;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.Util;
import org.springframework.stereotype.Component;

import static com.afrinnov.maabara.commons.PrefixCode.EXAMGROUP;

@Component
public class ExamGroupMapper extends AbstractMapper<ExamGroup, ExamGroupDto, ExamGroupForm> {
    @Override
    public ExamGroup fromDtoToEntity(ExamGroupDto examGroupDto) {
        ExamGroup examGroup = new ExamGroup();
        examGroup.setCode(examGroupDto.getExamGroupCode());
        examGroup.setDescription(examGroupDto.getDescription());
        examGroup.setLibelle(examGroupDto.getLibelle());

        return examGroup;
    }

    @Override
    public ExamGroup CopyDtoToEntity(ExamGroup examGroup, ExamGroupDto examGroupDto) {
        examGroup.setLibelle(examGroupDto.getLibelle());
        examGroup.setDescription(examGroupDto.getDescription());

        return examGroup;
    }

    @Override
    public ExamGroup fromFormToEntity(ExamGroupForm examGroupForm) {
        ExamGroup examGroup = new ExamGroup();
        examGroup.setCode(Util.generateCode(EXAMGROUP));
        examGroup.setDescription(examGroupForm.getDescription());
        examGroup.setLibelle(examGroupForm.getLibelle());

        return examGroup;
    }

    @Override
    public ExamGroupDto fromEntityToDto(ExamGroup examGroup) {
        ExamGroupDto examGroupDto = new ExamGroupDto();
        examGroupDto.setDeleted(examGroup.isDeleted());
        examGroupDto.setDescription(examGroup.getDescription());
        examGroupDto.setLibelle(examGroup.getLibelle());
        examGroupDto.setExamGroupCode(examGroup.getCode());

        return examGroupDto;
    }

    @Override
    public ExamGroup deleteEntity(ExamGroup examGroup) {
        examGroup.setDeleted(true);
        return examGroup;
    }
}
