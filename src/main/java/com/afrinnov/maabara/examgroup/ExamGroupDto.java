package com.afrinnov.maabara.examgroup;

import com.afrinnov.maabara.exam.ExamDto;
import java.util.HashSet;
import java.util.Set;

public class ExamGroupDto {
    private String libelle;
    private String description;
    private String examGroupCode;
    private boolean isDeleted;
    private Set<ExamDto> examsDto= new HashSet<>();

    public ExamGroupDto() {
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExamGroupCode() {
        return examGroupCode;
    }

    public void setExamGroupCode(String examGroupCode) {
        this.examGroupCode = examGroupCode;
    }

    public Set<ExamDto> getExamsDto() {
        return examsDto;
    }

    public void setExamsDto(Set<ExamDto> examsDto) {
        this.examsDto = examsDto;
    }
}
