package com.afrinnov.maabara.examgroup;

import com.afrinnov.maabara.commons.AbstractEntity;
import com.afrinnov.maabara.exam.Exam;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ExamGroup extends AbstractEntity {
    private String libelle;
    private String description;

    @OneToMany(mappedBy = "examGroup")
    @JsonIgnore
    private Set<Exam> exams= new HashSet<>();

    public ExamGroup() {
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

}
