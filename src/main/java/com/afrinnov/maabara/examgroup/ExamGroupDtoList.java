package com.afrinnov.maabara.examgroup;

public class ExamGroupDtoList {
    private String examGroupLabel;
    private String examGroupCode;
    private boolean isDeleted;

    public ExamGroupDtoList() {
    }

    public String getExamGroupLabel() {
        return examGroupLabel;
    }

    public void setExamGroupLabel(String examGroupLabel) {
        this.examGroupLabel = examGroupLabel;
    }

    public String getExamGroupCode() {
        return examGroupCode;
    }

    public void setExamGroupCode(String examGroupCode) {
        this.examGroupCode = examGroupCode;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
