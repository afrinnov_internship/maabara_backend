package com.afrinnov.maabara.examgroup;

import com.afrinnov.maabara.commons.AbstractMapper;
import com.afrinnov.maabara.commons.AbstractService;
import com.afrinnov.maabara.commons.IRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExamGroupService extends AbstractService<ExamGroup, ExamGroupDto, ExamGroupForm> {
    private  ExamGroupRepository examGroupRepository;
    private  ExamGroupMapper examGroupMapper;

    public ExamGroupService(ExamGroupRepository examGroupRepository, ExamGroupMapper examGroupMapper) {
        this.examGroupRepository = examGroupRepository;
        this.examGroupMapper = examGroupMapper;
    }

    @Override
    protected boolean checkAlreadyEntity() {
        return false;
    }

    @Override
    protected IRepository<ExamGroup> getRepository() {
        return this.examGroupRepository;
    }

    @Override
    protected AbstractMapper<ExamGroup, ExamGroupDto, ExamGroupForm> getMapper() {
        return this.examGroupMapper;
    }

    @Override
    public ExamGroupDto save(ExamGroupForm examGroupForm) {
        return super.save(examGroupForm);
    }

   /* @Override
    public List<ExamGroupDtoList> findAll() {

        List<ExamGroup> exams = this.examGroupRepository.findAll();
        List<ExamGroupDtoList> examGroupDtoLists = new ArrayList<>();
        for (int i=0; i<exams.size();i++){
            ExamGroupDtoList examGroupDtoList = new ExamGroupDtoList();
            examGroupDtoList.setDeleted(examGroupDtoLists.get(i).isDeleted());
            examGroupDtoList.setExamGroupCode(examGroupDtoLists.get(i).getExamGroupCode());
            examGroupDtoList.setExamGroupLabel(examGroupDtoLists.get(i).getExamGroupLabel());
            examGroupDtoLists.add(examGroupDtoList);
        }

        return examGroupDtoLists;
    }
     */

    public List<ExamGroupDtoList> findList() {

        List<ExamGroup> exams = this.examGroupRepository.findAll();
        List<ExamGroupDtoList> examGroupDtoLists = new ArrayList<>();
        for (int i=0; i<exams.size();i++){
            ExamGroupDtoList examGroupDtoList = new ExamGroupDtoList();
            examGroupDtoList.setDeleted(exams.get(i).isDeleted());
            examGroupDtoList.setExamGroupCode(exams.get(i).getCode());
            examGroupDtoList.setExamGroupLabel(exams.get(i).getLibelle());
            examGroupDtoLists.add(examGroupDtoList);
        }

        return examGroupDtoLists;
    }

    @Override
    public ExamGroupDto findByCode(String code) {
        return super.findByCode(code);
    }

    @Override
    public ExamGroupDto update(String code, ExamGroupDto examGroupDto) {
        return super.update(code, examGroupDto);
    }

    @Override
    public void delete(String code) {
        super.delete(code);
    }

    @Override
    public ExamGroupDto details(String code) {
        return super.details(code);
    }
}
