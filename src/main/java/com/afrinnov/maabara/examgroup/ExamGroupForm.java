package com.afrinnov.maabara.examgroup;

public class ExamGroupForm {
    private String libelle;
    private String description;

    public ExamGroupForm() {
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
