package com.afrinnov.maabara.examgroup;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/examGroup")
@CrossOrigin
public class ExamGroupController {

    private ExamGroupService examGroupService;

    public ExamGroupController(ExamGroupService examGroupService) {
        this.examGroupService = examGroupService;
    }

    @GetMapping("/list")
    public List<ExamGroupDtoList> listExamGroup(){
        return examGroupService.findList();
    }

    @PostMapping(value = "/add")
    public ExamGroupDto addExamGroup(@RequestBody ExamGroupForm examGroupForm){
        return this.examGroupService.save(examGroupForm);
    }

    @PostMapping(value = "/modify")
    public ExamGroupDto modifyExamGroup(@RequestBody ExamGroupDto examGroupDto){
        return this.examGroupService.update(examGroupDto.getExamGroupCode(), examGroupDto);
    }

    @DeleteMapping(value = "/delete")
    public void deleteExamGroup(@RequestParam String examGroupCode){
        this.examGroupService.delete(examGroupCode);
    }

    @PostMapping(value = "/details")
    public ExamGroupDto detailsExamGroup(@RequestParam String examGroupCode){
        return this.examGroupService.details(examGroupCode);
    }
}
