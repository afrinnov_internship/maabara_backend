package com.afrinnov.maabara.user;

import com.afrinnov.maabara.commons.AbstractEntity;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public abstract class User extends AbstractEntity {
    private String name;
    private String surname;
    private Date dateBirth;
    private String phone;
    private String sex;

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
