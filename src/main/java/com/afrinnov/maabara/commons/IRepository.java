package com.afrinnov.maabara.commons;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface IRepository<ENTITY> extends JpaRepository<ENTITY, Long> {
    Optional<ENTITY> findByCode(String code);

}
