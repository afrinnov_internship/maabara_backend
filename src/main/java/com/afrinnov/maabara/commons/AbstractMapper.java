package com.afrinnov.maabara.commons;

import java.util.Optional;

public abstract class AbstractMapper<ENTITY, DTO, FORM> {
    public abstract ENTITY fromDtoToEntity(DTO dto);
    public abstract ENTITY CopyDtoToEntity(ENTITY entity, DTO dto);
    public abstract ENTITY fromFormToEntity(FORM form);
    public abstract DTO fromEntityToDto(ENTITY entity);
    public abstract ENTITY deleteEntity(ENTITY entity);
}
