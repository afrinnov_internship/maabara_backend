package com.afrinnov.maabara.commons;

import com.afrinnov.maabara.consultation.ConsultationDto;
import com.afrinnov.maabara.consultation.ConsultationForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<ENTITY, DTO, FORM> {
    protected abstract boolean checkAlreadyEntity();
    protected abstract IRepository<ENTITY> getRepository();
    protected abstract AbstractMapper<ENTITY, DTO, FORM> getMapper();


    public DTO save(FORM form) {
        ENTITY entity = this.getMapper().fromFormToEntity(form);
        entity = getRepository().save(entity);
        DTO dto = this.getMapper().fromEntityToDto(entity);
        return dto;
    }

    public List<DTO> findAll() {
        List<ENTITY> entities = getRepository().findAll();
        List<DTO> dtoList = new ArrayList();
        for (int i=0; i<entities.size();i++){
            dtoList.add(this.getMapper().fromEntityToDto(entities.get(i)));
        }
        return dtoList;
    }

    public DTO findByCode(String code){
        Optional<ENTITY> entity = this.getRepository().findByCode(code);
        if(entity.isPresent()){
            return this.getMapper().fromEntityToDto(entity.get());
        }
        return null;
    }

    public DTO update(String code, DTO dto){
        Optional<ENTITY> entity = this.getRepository().findByCode(code);
        if(entity.isPresent()){
            ENTITY entity_get = entity.get();
            ENTITY entity_dto = this.getMapper().CopyDtoToEntity(entity_get, dto);
            entity_dto = getRepository().save(entity_dto);
            DTO dto_entity = this.getMapper().fromEntityToDto(entity_dto);
            return dto_entity;
        }
        return null;
    }

    public void delete(String code){
        Optional<ENTITY> entity = this.getRepository().findByCode(code);
        if(entity.isPresent()){
            ENTITY entity_delete = entity.get();
            entity_delete = this.getMapper().deleteEntity(entity_delete);
            entity_delete = getRepository().save(entity_delete);
        }
    }

    public DTO details(String code){
        Optional<ENTITY> entity = this.getRepository().findByCode(code);
        if(entity.isPresent()){
            ENTITY entity_get = entity.get();
            DTO dto = this.getMapper().fromEntityToDto(entity_get);
            return dto;
        }
        return null;
    }
}
