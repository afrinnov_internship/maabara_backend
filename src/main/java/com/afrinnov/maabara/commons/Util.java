package com.afrinnov.maabara.commons;

import java.util.UUID;

public class Util {

    public static String generateCode(PrefixCode prefixCode){
        return prefixCode.toString() + UUID.randomUUID().toString();
    }
}
