package com.afrinnov.maabara.commons;

public enum PrefixCode {
    CONSULTATION,
    EXAM,
    EXAMGROUP,
    SOUSCRITEXAM,
    PATIENT;

    @Override
    public String toString() {

        switch (this) {
            case CONSULTATION:
                return "CONSULT_";
            case EXAM:
                return "EXAM_";
            case PATIENT:
                return "PATIENT_";
            case EXAMGROUP:
                return "EXAMGROUP_";
            case SOUSCRITEXAM:
                return "SOUSCRITEXAM_";
            default:
                return null;
        }

    }
}
